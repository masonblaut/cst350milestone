﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Activity3a.Models
{
    public class ProductModel
    {
        [DisplayName("ID Number")]
        public int Id { get; set; }
        [DisplayName("Product Name")]
        public string Name { get; set; }
        [DisplayName("MSRP:")]
        public decimal Price { get; set; }
        [DisplayName("Product Dscription")]
        public string Description { get; set; }

    }
}
