﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Activity3a.Models
{
    public class MovieModel
    {
        [DisplayName("Movie Title")]
        public string Title { get; set; }
        [DisplayName("Rating")]
        public int Rating { get; set; }
        [DisplayName("Release Date")]
        public DateTime ReleaseDate { get; set; }
    }
}
