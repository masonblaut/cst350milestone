﻿using BibleVerses.Models;
using BibleVerses.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BibleVerses.Controllers
{
    public class VerseController : Controller
    {
        //Data Access Object available to all class methods
        VersesDAO vDAO = new VersesDAO(); 

        //Displays all verses returned from the DAO
        public IActionResult Index()
        {
            return View("Index", vDAO.GetAllVerses());
        }

        //Displays the search form view
        public IActionResult SearchForm()
        {
            return View();
        }

        //Loads the Verse Index view specifically awaiting a get request
        [HttpGet]
        public IActionResult SearchResults(string searchTerm, int search_section)
        {
            List<VerseModel> searchList = vDAO.SearchAllVerses(searchTerm, search_section);
            return View("Index", searchList);
        }
    }
}
