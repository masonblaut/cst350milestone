﻿using BibleVerses.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BibleVerses.Services
{
    public class VersesDAO : IVerseDataService
    {
        //connection string needed to locate db
        string connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Bible;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        //returns all verses in t_asv table
        public List<VerseModel> GetAllVerses()
        {
            List<VerseModel> foundVerses = new List<VerseModel>();
            string sqlStatement = "select * from dbo.t_asv";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sqlStatement, connection);

                try
                {
                    connection.Open();

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        foundVerses.Add(new VerseModel { Id = (int)reader[0], Book = (int)reader[1], Chapter = (int)reader[2], VerseNum = (int)reader[3], VerseText = (string)reader[4] });
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return foundVerses;
        }

        //returns list of verses filtered by the search term and bible section
        public List<VerseModel> SearchAllVerses(string searchTerm, int search_section)
        {
            List<VerseModel> foundVerses = new List<VerseModel>();
            string sqlStatement;

            if (search_section == 0)
                sqlStatement = "select * from dbo.t_asv WHERE t LIKE @Text";
            else if (search_section == 1)
                sqlStatement = "select * from dbo.t_asv WHERE b < 40 AND t LIKE @Text";
            else
                sqlStatement = "select * from dbo.t_asv WHERE b > 39 AND t LIKE @Text";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sqlStatement, connection);
                command.Parameters.AddWithValue("@Text", '%' + searchTerm + '%');

                try
                {
                    connection.Open();

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        foundVerses.Add(new VerseModel { Id = (int)reader[0], Book = (int)reader[1], Chapter = (int)reader[2], VerseNum = (int)reader[3], VerseText = (string)reader[4] });
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return foundVerses;
        }
    }
}
