﻿using BibleVerses.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BibleVerses.Services
{
    interface IVerseDataService
    {
        //Method to return full contents of the t_asv table
        List<VerseModel> GetAllVerses();
        //Method returning list of verses filtered by a search term in the text or the section of the bible it is located within
        List<VerseModel> SearchAllVerses(string searchTerm, int search_section);
    }
}
