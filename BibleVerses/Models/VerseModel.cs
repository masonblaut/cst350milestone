﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace BibleVerses.Models
{
    public class VerseModel
    {
        //Id number for each verse
        [DisplayName("ID Number")]
        public int Id { get; set; }
        //The book each verse is contained within, in chronological order
        [DisplayName("Book")]
        public int Book { get; set; }
        //The chapter each verse is contained within, in chronological order
        [DisplayName("Chapter")]
        public int Chapter { get; set; }
        //specified number for each verse
        [DisplayName("Verse Number")]
        public int VerseNum { get; set; }
        //verse text 
        [DisplayName("Text")]
        public string VerseText { get; set; }
    }
}
