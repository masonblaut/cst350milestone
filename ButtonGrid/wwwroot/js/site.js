﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.


// Write your JavaScript code.
$(function () {
    console.log("page is ready");
    /*
    $(document).on("click", ".game-button", function (event) {
        event.preventDefault();

        var buttonNumber = $(this).val();
        console.log("button" + buttonNumber + " was clicked");
        doButtonUpdate(buttonNumber);
    })*/

    $(document).on("mousedown", ".game-button", function (event) {
        event.preventDefault();

        switch (event.which) {
            case 1:
                var buttonNumber = $(this).val();
                console.log("button number " + buttonNumber + " was left clicked");
                doButtonUpdate(buttonNumber, 'button/ShowOneButton');
                break;
            case 2:
                alert("Middle Mouse button clicked");
                break;
            case 3:
                var buttonNumber = $(this).val();
                console.log("button number " + buttonNumber + " was right clicked");
                doButtonUpdate(buttonNumber, 'button/RightClickShowOneButton');
                break;
            default:
                alert("nothing");
        }
    })

    $(document).bind("contextmenu", function (e) {
        e.preventDefault();
        console.log("right click, prevented context menu");
    })

    function doButtonUpdate(buttonNumber, urlString) {
        $.ajax({
            datatype: "json",
            method: 'POST',
            url: urlString,
            data: {
                "buttonNumber": buttonNumber
            },
            success: function (data) {
                console.log(data);
                $("#" + buttonNumber).html(data);
            }
        })
    }
        
});