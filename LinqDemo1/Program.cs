﻿using System;
using System.Linq;

namespace LinqDemo1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] scores = { 50, 60, 80, 81, 0, 100, 99, 72, 87, 85, 81, 24, 18 };

            foreach( var item in scores )
            {
                Console.WriteLine("score: "+ item);
            }
            Console.WriteLine();
            

            //use linq to filter the list
            var theBestStudents =
                from item in scores
                where item > 90
                select item;
            foreach (var item in theBestStudents)
            {
                Console.WriteLine("Highest Scores: " + item);
            }
            Console.WriteLine();

            var sortedScores =
                from item in scores
                orderby item descending
                select item;
            Console.WriteLine("Sorted scores:");
            foreach (var item in sortedScores)
            {
                Console.WriteLine(" score: " + item);
            }
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
