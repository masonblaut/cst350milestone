﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace VideoGameDependencyExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            HeroThatOnlyUsesSwords hero1 = new HeroThatOnlyUsesSwords { Name = "King Arthur" };
            hero1.Attack();
            Console.WriteLine();

            HeroThatCanUseAnyWeapon hero2 = new HeroThatCanUseAnyWeapon { Name = "Eragon", MyWeapon = new Sword { SwordName = "Brisinger" } };
            hero2.Attack();
            Console.WriteLine();

            HeroThatCanUseAnyWeapon hero3 = new HeroThatCanUseAnyWeapon { Name = "The Joker", MyWeapon = new Granade { Name = "The Pineapple" } };
            hero3.Attack();
            Console.WriteLine();

            HeroThatCanUseAnyWeapon hero4 = new HeroThatCanUseAnyWeapon
            {
                Name = "GI Joe",
                MyWeapon = new Gun
                {
                    Name = "M16",
                    Bullets = new List<Bullet>
                    { new Bullet {Name = "Silver Slug", GramsOfPowder = 10},
                    new Bullet {Name = "Snake Shot", GramsOfPowder = 10},
                    new Bullet {Name = "Tracer Round", GramsOfPowder = 10},
                    new Bullet {Name = "Explosive Round", GramsOfPowder = 10},
                    }
                }
            };

            hero4.Attack();
            hero4.Attack();
            hero4.Attack();
            hero4.Attack();
            hero4.Attack();
            Console.WriteLine();

            ServiceCollection services = new ServiceCollection();

            //all new weapons will now be set to greanade byt default
            services.AddTransient<Iweapon, Gun>(s => new Gun
            {
                Name = "M16",
                Bullets = new List<Bullet>
                    { new Bullet {Name = "Silver Slug", GramsOfPowder = 10},
                    new Bullet {Name = "Snake Shot", GramsOfPowder = 10},
                    new Bullet {Name = "Tracer Round", GramsOfPowder = 10},
                    new Bullet {Name = "Explosive Round", GramsOfPowder = 10},
                    }
            });

            //All new heroes will be Bond by default
            services.AddTransient<IHero, HeroThatCanUseAnyWeapon> (
                hero => new HeroThatCanUseAnyWeapon { Name = "James Bond", MyWeapon = hero.GetService<Iweapon>() });

            //compile step
            ServiceProvider provider = services.BuildServiceProvider();

            //implement
            var hero5 = provider.GetService<IHero>();

            //test
            hero5.Attack();
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
