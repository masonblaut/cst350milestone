﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoGameDependencyExample
{
    class HeroThatCanUseAnyWeapon : IHero
    {
        public string Name { get; set; }
        public Iweapon MyWeapon { get; set; }

        public void Attack()
        {
            Console.WriteLine(Name + " prepares to attack");
            MyWeapon.AttackWithMe();
        }
    }
}
