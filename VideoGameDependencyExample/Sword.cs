﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoGameDependencyExample
{
    class Sword : Iweapon
    {
        public string SwordName { get; set; }

        public void AttackWithMe()
        {
            Console.WriteLine(SwordName + " attacks all enemies");
        }
    }
}
