﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoGameDependencyExample
{
    class Gun : Iweapon
    {
        public string Name { get; set; }
        public List<Bullet> Bullets { get; set; }

        public void AttackWithMe()
        {
            if (Bullets.Count > 0)
            {
                Console.WriteLine(Name + " fires the round called " + Bullets[0].Name);
                Bullets.RemoveAt(0);
            }
            else
            {
                Console.WriteLine("The weapon is out of ammo!");
            }
        }
    }
}
