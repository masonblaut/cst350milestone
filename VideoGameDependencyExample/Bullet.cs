﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoGameDependencyExample
{
    class Bullet
    {
        public string Name { get; set; }
        public int GramsOfPowder { get; set; }
    }
}
