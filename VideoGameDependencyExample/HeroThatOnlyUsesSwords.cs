﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoGameDependencyExample
{
    class HeroThatOnlyUsesSwords
    {
        public string Name { get; set; }
        public void Attack()
        {
            Sword sword = new Sword { SwordName = "Exclaibur" };
            Console.WriteLine(Name + " prepares himself for battle");
            sword.AttackWithMe();
        }
    }
}
