﻿using Activity3a.Models;
using Activity3a.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Activity2.Controllers
{
    public class ProductsController : Controller
    {
        ProductsDAO repository;
        public ProductsController()
        {
            repository = new ProductsDAO();
        }
        public IActionResult Index()
        {
            return View(repository.GetAllProducts());
        }

        public IActionResult SearchResults(string searchTerm)
        {

            List<ProductModel> productList = repository.SearchAllProducts(searchTerm);
            return View("Index", productList);
        }

        public IActionResult ShowDetails(int id)
        {
            ProductModel foundProduct = repository.GetProductById(id);
            return View(foundProduct);
        }

        public IActionResult ShowOneProductJSON(int Id)
        {
            return Json(repository.GetProductById(Id));
        }

        public IActionResult Edit(int id)
        {
            ProductModel foundProduct = repository.GetProductById(id);
            return View("ShowEdit", foundProduct);
        }

        public IActionResult ProcessEdit(ProductModel product)
        {
            repository.Update(product);
            return View("Index", repository.GetAllProducts());
        }

        public IActionResult ProcessEditReturnPartial(ProductModel product)
        {
            repository.Update(product);
            return PartialView("_productCard", product);
        }

        public IActionResult Delete(int id)
        {
            ProductModel product = repository.GetProductById(id);
            repository.Delete(product);
            return View("Index", repository.GetAllProducts());
        }

        public IActionResult SearchForm()
        {
            return View();
        }

        public IActionResult Message()
        {
            return View("Message");
        }

        public IActionResult Welcome(string name, int secretNumber=13)
        {
            ViewBag.Name = name;
            ViewBag.SecretNumber = secretNumber;
            return View();
        }
    }
}
