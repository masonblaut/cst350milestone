﻿using Activity3a.Models;
using Activity3a.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace Activity2.Controllers
{
    [ApiController]
    [Route("api/")]
    public class ProductsControllerAPI : ControllerBase
    {
        ProductsDAO repository;
        public ProductsControllerAPI()
        {
            repository = new ProductsDAO();
        }

        [HttpGet]
        [ResponseType(typeof(List<ProductModelDTO>))]
        public  IEnumerable<ProductModelDTO> Index()
        {
            List<ProductModel> products = repository.GetAllProducts();
            //List<ProductModelDTO> proDTOs = new List<ProductModelDTO>();

            IEnumerable<ProductModelDTO> productModelDTOs = from p in products select new ProductModelDTO(p);

            /*
            foreach (var p in products)
            {
                proDTOs.Add(new ProductModelDTO(p));
            }
            */


            return productModelDTOs;
        }
        
        [HttpGet("searchproducts/{searchTerm}")]
        public ActionResult <IEnumerable<ProductModel>> SearchProducts(string searchTerm)
        {

            List<ProductModel> productList = repository.SearchAllProducts(searchTerm);

            return productList;
        }
        
        [HttpGet("ShowOneProduct/{Id}")]
        public ActionResult <ProductModelDTO> ShowOneProduct(int id)
        {
            ProductModel p = repository.GetProductById(id);
            ProductModelDTO pDTO = new ProductModelDTO(p);
            return pDTO;
        }

        [HttpPost("insertOne")]
        public ActionResult <int> InsertOne(ProductModel product)
        {
            int newId = repository.Insert(product);
            return newId;
        }

        [HttpPut("ProcessEdit")]
        public ActionResult <ProductModel> ProcessEdit(ProductModel product)
        {
            repository.Update(product);
            return repository.GetProductById(product.Id);
        }

        [HttpDelete("DeleteOne/{Id}")]
        public ActionResult <int> DeleteOne(int id)
        {
            ProductModel product = repository.GetProductById(id);
            int success = repository.Delete(product);
            return success;
        }

    }
}
