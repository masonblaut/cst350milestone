﻿using Activity3a.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Activity3a.Controllers
{
    public class MovieController : Controller
    {
        public IActionResult Index()
        {
            MovieHardcodeSample mhs = new MovieHardcodeSample();

            return View(mhs.GetAllMovies());
        }
    }
}
