﻿using Activity3a.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Activity3a.Services
{
    public class MovieHardcodeSample
    {
        public List<MovieModel> GetAllMovies()
        {
            List <MovieModel> movieList = new List<MovieModel>();

            if (movieList.Count == 0)
            {
                movieList.Add(new MovieModel { Title = "Star Wars", Rating = 5, ReleaseDate = new DateTime(1977, 5, 25) });
                movieList.Add(new MovieModel { Title = "Lord of the Rings", Rating = 5, ReleaseDate = new DateTime(2001, 12, 19) });
                movieList.Add(new MovieModel { Title = "Movie 3", Rating = 2, ReleaseDate = new DateTime(2021, 1, 1) });
            }


            return movieList;
        }
    }
}
