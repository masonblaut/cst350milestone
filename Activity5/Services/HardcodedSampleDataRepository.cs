﻿using Activity3a.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Activity3a.Services
{
    public class HardcodedSampleDataRepository : IProductDataService
    {
        List<ProductModel> productList = new List<ProductModel>();

        public int Delete(ProductModel product)
        {
            throw new NotImplementedException();
        }

        public List<ProductModel> GetAllProducts()
        {
            if (productList.Count == 0)
            {
                productList.Add(new ProductModel { Id = 1, Name = "mouse Pad", Price = 5.99m, Description = "Its literally just a mouse pad" });
                productList.Add(new ProductModel { Id = 2, Name = "mouse Pad2", Price = 5.99m, Description = "Its literally just a mouse pad" });
                productList.Add(new ProductModel { Id = 3, Name = "mouse Pad3", Price = 5.99m, Description = "Its literally just a mouse pad" });
                productList.Add(new ProductModel { Id = 4, Name = "mouse Pad4", Price = 5.99m, Description = "Its literally just a mouse pad" });
            }
            

            return productList;
        }

        public ProductModel GetProductById(int id)
        {
            throw new NotImplementedException();
        }

        public int Insert(ProductModel product)
        {
            throw new NotImplementedException();
        }

        public List<ProductModel> SearchAllProducts(string searchTerm)
        {
            throw new NotImplementedException();
        }

        public int Update(ProductModel product)
        {
            throw new NotImplementedException();
        }
    }
}
